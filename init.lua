-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

local M = {
	auto_square_brackets = false,
	lispwords = {},
	repl_fifo = false,
	emacs = false,
	compat = false,
}

local textadept = require"textadept"
local events = require"events"
local keys = require"keys"

local ui = ui
local CURSES = CURSES

-- copied from core/keys.lua
local CTRL, ALT --[[, CMD]], SHIFT = 'ctrl+', not CURSES and 'alt+' or 'meta+' --[[, 'cmd+']], 'shift+'

-- XXX: in Lua 5.2 unpack() was moved into table
local unpack = table.unpack or unpack

local env = {}

local cwd = ...
local init, supported = unpack((require(cwd..".parkour")))

local do_copy = false
local initialized = false
local orig = {}
local errors = {}

setmetatable(M, {__newindex = function(_, k)
	local options = {}
	for option, _ in pairs(M) do
		table.insert(options, option)
	end
	table.sort(options)
	table.insert(errors, ([[%s: unrecognized option '%s'
	 Possible values: %s]]):format(cwd, k, table.concat(options, ", ")))
end})

setmetatable(M.lispwords, {__index = function(t, filetype)
	if supported[filetype] then
		local recfg = {__newindex = function(_, k, v)
			events.connect(events.FILE_OPENED, function()
				if filetype == buffer.lexer_language then
					env[buffer].lispwords[k] = v
				end
			end)
		end}
		t[filetype] = setmetatable({}, recfg)
		return t[filetype]
	end
end})

local H = {M = {}, T = {}, O = {}, I = {}, A = {}}  -- handler functions
local B = {M = {}, T = {}, I = {}}  -- bindings
local new = {}  -- constructors

local function range_by_pos(pos)
	return {start = pos, finish = pos}
end

function H.A.paste_reindent()
	local clipboard = ui.clipboard_text
	if not (clipboard and #clipboard > 0) then return end
	buffer:begin_undo_action()
	buffer:paste()
	local pos = buffer.selection_n_caret[1] - 1
	local cursor = range_by_pos(pos)
	local _, parent = env[buffer].walker.sexp_at(cursor, true)
	env[buffer].edit.refmt_at(parent, cursor)
	buffer:end_undo_action()
end

local function is_comment(t) return t.is_comment end
local function startof(t) return t.start end

function H.M.backward(range)
	return env[buffer].walker.start_before(range, is_comment)
end

function H.M.forward(range)
	return env[buffer].walker.finish_after(range, is_comment)
end

function H.M.forward_up(range)
	return env[buffer].walker.finish_up_after(range)
end

function H.M.forward_down(range)
	return env[buffer].walker.start_down_after(range, is_comment)
end

function H.M.backward_up(range)
	return env[buffer].walker.start_up_before(range)
end

function H.M.backward_down(range)
	return env[buffer].walker.finish_down_before(range, is_comment)
end

function H.M.beginning_of_sentence(range)
	return env[buffer].walker.anylist_start(range)
end

function H.M.end_of_sentence(range)
	return env[buffer].walker.anylist_finish(range)
end

function H.M.next_section(range)
	local newpos = env[buffer].walker.find_after(range, function(t) return t.section and t.start > range.start end)
	return newpos and newpos.start or buffer.length
end

function H.M.prev_section(range)
	local newpos = env[buffer].walker.find_before(range, function(t) return t.section end)
	return newpos and newpos.start or 0
end

function H.M.para_up(range)
        local sexp = env[buffer].parser.tree.before(range.start, startof, is_comment)
        return sexp and sexp.start
end

function H.M.para_down(range)
        local parent = env[buffer].walker.paragraph_at(range)
        local sexp = env[buffer].parser.tree.after(parent and parent.finish or range.finish, startof, is_comment)
        return sexp and sexp.start
end

-- This motion is only for use with delete
function H.M.backward_word(range)
	-- XXX: ignore the second value:
	return (env[buffer].walker.start_float_before(range, true))
end

-- This motion is only for use with delete
function H.M.forward_word(range)
	-- XXX: ignore the second value:
	return (env[buffer].walker.finish_float_after(range, true))
end

function H.M.prev_start(range)
	return env[buffer].walker.start_before(range, is_comment)
end

function H.M.prev_start_float(range)
        -- XXX: ignore the second value:
	return (env[buffer].walker.start_float_before(range))
end

function H.M.next_finish(range)
	return env[buffer].walker.finish_after(range, is_comment)
end

function H.M.next_finish_float(range)
        -- XXX: ignore the second value:
	return (env[buffer].walker.finish_float_after(range))
end

local function line_end(range)
	-- XXX: ignore the second value:
	return (env[buffer].walker.next_finish_wrapped(range))
end

function H.T.line_end_extend(range)
	return range.start, line_end(range)
end

local function line_begin(range)
	return env[buffer].walker.prev_start_wrapped(range)
end

function H.T.home_extend(range)
	return line_begin(range), range.finish
end

function H.M.back_to_indentation(_, pos)
	return buffer.line_indent_position[buffer:line_from_position(pos + 1)] - 1
end

function H.T.mark_sexp_backwards(range)
	local start = H.M.prev_start(range)
	if start then
		return start, range.finish
	end
end

function H.T.mark_sexp(range, pos)
	local backward = pos < range.finish
	if backward then
		local start = H.T.mark_sexp_backwards(range)
		return range.finish, start
	end
	local finish = H.M.next_finish(range)
	if finish then
		return range.start, finish
	end
end

function H.T.paragraph(range)
	local sexps = env[buffer].walker.repl_line_at(range)
	if sexps then
		return sexps.start, sexps.finish + 1
	end
end

function H.T.mark_defun(range)
	local sexp = env[buffer].walker.paragraph_at(range)
	if sexp and not sexp.is_comment then
		return sexp.start, sexp.finish + 1
	end
end

function H.T.expand_region(range)
	return env[buffer].walker.wider_than(range)
end

local function eval_repl_line(range, pos)
	local sexps = env[buffer].walker.repl_line_at(range)
	return sexps and H.O.eval_defun({start = sexps.start, finish = pos}, pos)
end

function H.O.format(range, pos)
	local cursor = range_by_pos(pos)
	-- XXX: don't call range_by_pos() from the argument list, as it returns a second value
	return env[buffer].edit.refmt_at(range, cursor) or pos
end

local function make_yank(handler, fragments)
	return function(range)
		local txt = buffer:text_range(range.start + 1, range.finish + 1)
		if txt then
			table.insert(fragments, 1, txt)
		end
		return handler and handler(range)
	end
end

local function _delete(range)
	return buffer:delete_range(range.start + 1, range.finish - range.start)
end

function H.O.delete(range, pos)
	local fragments = {}
	local delete_maybe_yank = do_copy and make_yank(_delete, fragments) or _delete
	local splicing = true
	return env[buffer].edit.delete_splicing(range, pos, splicing, delete_maybe_yank), fragments
end

function H.O.change(range, pos)
	local fragments = {}
	local delete_maybe_yank = do_copy and make_yank(_delete, fragments) or _delete
	return env[buffer].edit.delete_nonsplicing(range, pos, delete_maybe_yank), fragments
end

function H.O.yank(range, pos)
	local fragments = {}
	local yank = make_yank(nil, fragments)
	local action = {kill = false, wrap = true, splice = false, func = yank}
	env[buffer].edit.pick_out(range, pos, action)
	return range.start, fragments
end

local function backward_char(range)
	return buffer.position_before(range.start + 1) - 1
end

local function forward_char(range)
	return buffer.position_after(range.start + 1) - 1
end

function H.O.wrap_round(range, pos)
	return env[buffer].edit.wrap_round(range, pos, M.auto_square_brackets)
end

function H.O.wrap_square(range, pos)
	return env[buffer].edit.wrap_square(range, pos, M.auto_square_brackets)
end

function H.O.wrap_curly(range, pos)
	return env[buffer].edit.wrap_curly(range, pos, M.auto_square_brackets)
end

function H.O.meta_doublequote(range, pos)
	return env[buffer].edit.meta_doublequote(range, pos, M.auto_square_brackets)
end

function H.O.wrap_comment(range, pos)
	return env[buffer].edit.wrap_comment(range, pos)
end

function H.O.toggle_comment(range, pos)
	local sexp = env[buffer].walker.sexp_at(range, true)
	if sexp and not sexp.is_comment then
		return env[buffer].edit.wrap_comment({start = sexp.start, finish = sexp.finish + 1}, pos)
	elseif sexp then
		return env[buffer].edit.splice_sexp(range, pos)
	end
end

function H.O.close_and_newline(range)
	return env[buffer].edit.close_and_newline(range)
end

function H.O.open_next_line(range)
	return env[buffer].edit.close_and_newline(range, "\n")
end

function H.O.raise_sexp(range, pos)
	return env[buffer].edit.raise_sexp(range, pos) or pos
end

function H.O.splice_sexp(range, pos)
	return env[buffer].edit.splice_anylist(range, pos)
end

function H.O.cycle_wrap(range, pos)
	return env[buffer].edit.cycle_wrap(range, pos)
end

function H.O.forward_slurp(range)
	return env[buffer].edit.slurp_sexp(range, true)
end

function H.O.backward_slurp(range)
	return env[buffer].edit.slurp_sexp(range)
end

function H.O.forward_barf(range)
	return env[buffer].edit.barf_sexp(range, true)
end

function H.O.backward_barf(range)
	return env[buffer].edit.barf_sexp(range)
end

function H.O.split_sexp(range, pos)
	return env[buffer].edit.split_anylist(range) or pos
end

function H.O.join_sexps(range, pos)
	return env[buffer].edit.join_anylists(range) or pos
end

function H.O.convolute_sexp(range)
	return env[buffer].edit.convolute_lists(range)
end

function H.O.transpose_sexps(range, pos)
	return env[buffer].edit.transpose_sexps(range) or pos
end

function H.O.transpose_words(range, pos)
	return env[buffer].edit.transpose_words(range) or pos
end

function H.O.transpose_chars(range, pos)
	return env[buffer].edit.transpose_chars(range) or pos
end

local repl_fifo

function H.O.eval_defun(range, pos)
	if not M.repl_fifo or range.finish == range.start then return pos end
	if pos > range.finish then return pos end
	local unbalanced = env[buffer].parser.tree.unbalanced_delimiters(range)
	if unbalanced and #unbalanced > 0 then return pos end
	local errmsg
	if not repl_fifo then
		repl_fifo, errmsg = io.open(M.repl_fifo, "a+")
	end
	if repl_fifo then
		repl_fifo:write(buffer:text_range(range.start + 1, range.finish + 1), "\n")
		repl_fifo:flush()
	elseif errmsg then
		ui.print(errmsg)
	end
	return pos
end

-- flush any code stuck in the fifo, so starting a REPL after the file has been closed won't read old stuff in.
events.connect(events.QUIT, function()
	if repl_fifo then
		repl_fifo:close()
		repl_fifo = io.open(M.repl_fifo, "w+")
                if repl_fifo then
                        repl_fifo:close()
                        repl_fifo = nil
                end
	end
end)

H.I.cut                          = {H.O.change, do_copy = true}
H.I.copy                         = {H.O.yank, do_copy = true --[[XXX: just so the clipboard is emptied first]]}
H.I.backward_delete              = {H.O.change, backward_char}
H.I.forward_delete               = {H.O.change, forward_char}
H.I.backward_kill_word           = {H.O.change, H.M.backward_word, do_copy = true}
H.I.forward_kill_word            = {H.O.change, H.M.forward_word, do_copy = true}
H.I.backward_kill_sexp           = {H.O.change, H.M.prev_start, do_copy = true}
H.I.backward_kill_sexp1          = {H.O.change, H.M.prev_start}
H.I.backward_kill_sexp1_float    = {H.O.change, H.M.backward_word}
H.I.kill_sexp                    = {H.O.change, H.M.next_finish, do_copy = true}
H.I.kill_sexp1                   = {H.O.change, H.M.next_finish}
H.I.kill_sexp1_float             = {H.O.change, H.M.forward_word}
H.I.kill_sentence                = {H.O.change, H.M.end_of_sentence, do_copy = true}
H.I.backward_kill_sentence       = {H.O.change, H.M.beginning_of_sentence, do_copy = true}
H.I.backward_kill_line           = {H.O.change, line_begin, do_copy = true}
H.I.backward_kill_line1          = {H.O.change, line_begin}
H.I.kill                         = {H.O.change, line_end, do_copy = true}
H.I.kill1                        = {H.O.change, line_end}
H.I.splice_sexp_killing_backward = {H.O.delete, H.M.backward_up, do_copy = true}
H.I.splice_sexp_killing_backward1= {H.O.delete, H.M.backward_up}
H.I.splice_sexp_killing_forward  = {H.O.delete, H.M.forward_up, do_copy = true}
H.I.splice_sexp_killing_forward1 = {H.O.delete, H.M.forward_up}
H.I.raise_sexp                   = {H.O.raise_sexp}
H.I.splice_sexp                  = {H.O.splice_sexp}
H.I.forward_slurp                = {H.O.forward_slurp}
H.I.forward_barf                 = {H.O.forward_barf}
H.I.backward_slurp               = {H.O.backward_slurp}
H.I.backward_barf                = {H.O.backward_barf}
H.I.convolute_sexp               = {H.O.convolute_sexp}
H.I.wrap_round                   = {H.O.wrap_round, H.M.next_finish}
H.I.wrap_square                  = {H.O.wrap_square, H.M.next_finish}
H.I.wrap_curly                   = {H.O.wrap_curly, H.M.next_finish}
H.I.meta_doublequote             = {H.O.meta_doublequote, H.M.next_finish}
H.I.wrap_comment                 = {H.O.wrap_comment, H.M.next_finish}
H.I.enclose_round                = {H.O.wrap_round, H.T.expand_region}
H.I.enclose_square               = {H.O.wrap_square, H.T.expand_region}
H.I.enclose_curly                = {H.O.wrap_curly, H.T.expand_region}
H.I.enclose_doublequote          = {H.O.meta_doublequote, H.T.expand_region}
H.I.block_comment                = {H.O.toggle_comment}
H.I.close_and_newline            = {H.O.close_and_newline}
H.I.open_next_line               = {H.O.open_next_line}
H.I.split_sexp                   = {H.O.split_sexp}
H.I.join_sexps                   = {H.O.join_sexps}
H.I.transpose_sexps              = {H.O.transpose_sexps}
H.I.transpose_words              = {H.O.transpose_words}
H.I.transpose_chars              = {H.O.transpose_chars}
H.I.reindent_defun               = {H.O.format, H.T.mark_defun}
H.I.eval_defun                   = {H.O.eval_defun, H.T.paragraph}
H.I.eval_last_sexp               = {H.O.eval_defun, H.M.backward}

local function seek(selection)
	return function(offset)
		buffer.selection_n_caret[selection] = offset + 1
		buffer.selection_n_anchor[selection] = offset + 1
	end
end

function new.M(func)
	return function()
		for i = 1, buffer.selections do
			local pos = buffer.selection_n_caret[i] - 1
			seek(i)(func(range_by_pos(pos), pos) or pos)
		end
		buffer:scroll_range(buffer.selection_n_caret[1], buffer.selection_n_caret[buffer.selections])
		if func == H.M.prev_section or func == H.M.next_section then
			local align = buffer:line_from_position(buffer.current_pos) - buffer.first_visible_line
			buffer.line_scroll(0, align)
		end
		return true
	end
end

function new.T(textobject)
	return function()
		for i = buffer.selections, 1, -1 do
			local range = {buffer.selection_n_anchor[i] - 1, buffer.selection_n_caret[i] - 1}
			local pos = range[2]
			table.sort(range)
			range.start, range.finish = range[1], range[2]
			range[1], range[2] = nil, nil
			local start, finish = textobject(range, pos)
			if start and finish then
				if buffer.selections == 1 then
					buffer:set_selection(finish + 1, start + 1)
				else
					buffer:add_selection(finish + 1, start + 1)
				end
			end
		end
	end
end

function new.I(params)
	local operator, motion_or_textobject = unpack(params)
	return function()
		buffer:begin_undo_action()
		if params.do_copy then do_copy = true end
		local clips = {}
		for i = buffer.selections, 1, -1 do
			local pos = buffer.selection_n_caret[i] - 1
			local anchor = buffer.selection_n_anchor[i] - 1
			local range = {start = math.min(anchor, pos), finish = math.max(anchor, pos)}
			if motion_or_textobject and (range.finish == range.start) then
				local start, finish = motion_or_textobject(range, pos)
				if start and not finish then
					range = {start = math.min(pos, start), finish = math.max(pos, start)}
				elseif start and finish then
					range = {start = start, finish = finish}
				end
			end
			local newpos, fragments = operator(range, pos)
			if params.do_copy and fragments and #fragments > 0 then
				for _, v in ipairs(fragments) do
					table.insert(clips, v)
				end
			end
			seek(i)(newpos or pos)
		end
		do_copy = false
		if params.do_copy and #clips > 0 then
			local clipboard = table.concat(clips)
			buffer:copy_text(clipboard)
		end
		buffer:end_undo_action()
		return true
	end
end

local function mnemonics_off()
	if CURSES then return end
	if not initialized then return end -- wait for the storm of events to pass
	local filetype = buffer.lexer_language
	if not supported[filetype] then return end
        orig.menus = orig.menus or {}
	for _, menu in ipairs(textadept.menu.menubar) do
		local mnemonic, magic = menu.title:match"(([_&])%a)"
		mnemonic = mnemonic and mnemonic:sub(2, 2):lower()
		if mnemonic and keys[filetype] and keys[filetype][ALT..mnemonic] then
			local new_title = menu.title:gsub(magic, "", 1)
                        orig.menus[new_title] = menu.title
                        menu.title = new_title
		end
	end
end

local function mnemonics_on()
	if CURSES then return end
	if not initialized then return end -- wait for the storm of events to pass
	local filetype = buffer.lexer_language
	if not supported[filetype] then return end
	for _, menu in ipairs(textadept.menu.menubar) do
		if orig.menus[menu.title] then
			menu.title = orig.menus[menu.title]
		end
	end
end

local extended_identifier_chars = "!$%&*+-./:<=>?@^_~"

local function settings_backup()
	if not initialized then return end -- wait for the storm of events to pass
	local filetype = buffer.lexer_language
	if not supported[filetype] then return end
	if not buffer.word_chars:find(extended_identifier_chars, 1, true) then
		buffer.word_chars = extended_identifier_chars .. buffer.word_chars
	end
	orig.auto_indent = textadept.editing.auto_indent
	orig.strip_trailing_spaces = textadept.editing.strip_trailing_spaces
	orig.auto_pairs = textadept.editing.auto_pairs
	orig.typeover_auto_paired = textadept.editing.typeover_auto_paired

	textadept.editing.auto_indent = false
	textadept.editing.strip_trailing_spaces = false
	textadept.editing.auto_pairs = nil
	textadept.editing.typeover_auto_paired = nil
end

local function settings_restore()
	if not initialized then return end -- wait for the storm of events to pass
	local filetype = buffer.lexer_language
	if not supported[filetype] then return end
	textadept.editing.auto_indent = orig.auto_indent
	textadept.editing.strip_trailing_spaces = orig.strip_trailing_spaces
	textadept.editing.auto_pairs = orig.auto_pairs
	textadept.editing.typeover_auto_paired = orig.typeover_auto_paired
end

local function setup() mnemonics_off() settings_backup() end
local function teardown() mnemonics_on() settings_restore() end

events.connect(events.BUFFER_BEFORE_SWITCH, function() teardown() end)
events.connect(events.BUFFER_AFTER_SWITCH, function() setup() end)
events.connect(events.VIEW_BEFORE_SWITCH, function() teardown() end)
events.connect(events.VIEW_AFTER_SWITCH, function() setup() end)

events.connect(events.INITIALIZED, function()
        for _, errmsg in ipairs(errors) do
                ui.print(errmsg)
        end
	for kind, register_new in pairs(new) do
		for name, handler in pairs(H[kind]) do
			B[kind][name] = register_new(handler)
		end
	end
	local suffix = CURSES and "-curses" or ""
	local keyconfig = {require(cwd..".keytheme.CUA"..suffix)(B, H)}
        if M.compat then
                table.insert(keyconfig, require(cwd..".keytheme.CUA-compat"..suffix)(B, H))
        end
        if M.emacs then
                table.insert(keyconfig, require(cwd..".keytheme.emacs"..suffix)(B, H))
        end
	local inverted = {}
	for _, config in ipairs(keyconfig) do
		for func, key in pairs(config) do
			if type(key) == "table" then
				for _, k in ipairs(key) do
					inverted[k] = func
				end
				for k, j in pairs(key) do
					if type(k) ~= "number" and (not inverted[k] or type(inverted[k]) ~= "table") then
						inverted[k] = {}
					end
					if type(inverted[k]) == "table" then
						inverted[k][j] = func
					end
				end
			else
				inverted[key] = func
			end
		end
	end
	for dialect in pairs(supported) do
		if not keys[dialect] then keys[dialect] = {} end
		for key, binding in pairs(inverted) do
			keys[dialect][key] = binding
		end
	end
	initialized = true
	setup()
end)

events.connect(events.FILE_OPENED, function()
	local filetype = buffer.lexer_language
	if not supported[filetype] then return end
	local function write(pos, txt)
		buffer:set_target_range(pos + 1, pos + 1)
		buffer:replace_target(txt)
	end
	local function delete(pos, len)
		return buffer:delete_range(pos + 1, len)
	end
	local function read(base, len)
		if base == buffer.length - 1 then return end
		base = (base or 0) + 1
		len = len or buffer.length
		local more = base + len <= buffer.length
		return buffer:text_range(base, base + len), more
	end
	-- TODO: this can be implemented in the core modules, without using editor-specific APIs:
	local function eol_at(pos)
		return buffer.line_end_position[buffer:line_from_position(pos + 1)] - 1
	end
	-- TODO: this can be implemented in the core modules, without using editor-specific APIs:
	local function bol_at(pos)
		return buffer:position_from_line(buffer:line_from_position(pos + 1)) - 1
	end
	env[buffer] = init(filetype, read, write, delete, eol_at, bol_at)
	local consumed_char = {"\n", " "}
	for ch in pairs(env[buffer].parser.opposite) do
		if #ch == 1 then
			table.insert(consumed_char, ch)
		end
	end
	env[buffer].consumed_char = "[" .. table.concat(consumed_char, "%") .. "]"
	setup()
end)

events.connect(events.BUFFER_DELETED, function()
	for b in pairs(env) do
		if not _BUFFERS[b] then
			env[b] = nil
		end
	end
end)

events.connect(events.MODIFIED, function(pos, mtype)
	local filetype = buffer.lexer_language
	if not supported[filetype] then return end
	local text_inserted, text_deleted = 0x01, 0x02
	if 0 == (mtype & (text_inserted | text_deleted)) then return end
	if env[buffer].parser.tree.is_parsed(pos - 1) then
		env[buffer].parser.tree.rewind(pos - 1)
	end
end)

events.connect(events.KEYPRESS, function(key)
	if ui.command_entry.active then return end
	local filetype = buffer.lexer_language
	if not supported[filetype] then return end
        if key:find(CTRL) or key:find(ALT) or key:find(SHIFT) then return end
	local ret
	local winput = env[buffer].input
	local consumed_char = key:find(env[buffer].consumed_char)
	if consumed_char then buffer:begin_undo_action() end
	for i = buffer.selections, 1, -1 do
		local anchor, caret = buffer.selection_n_anchor[i] - 1, buffer.selection_n_caret[i] - 1
		local range = {start = math.min(anchor, caret), finish = math.max(anchor, caret)}
		if key == "\n" then
			local sexp, parent = env[buffer].walker.sexp_at(range, true)
			if parent.is_root and (not sexp or (not sexp.is_comment and sexp.finish + 1 == range.start)) then
				local same_line = not not sexp
				if not sexp then
					-- FIXME: this assumes only one selection:
					local line, pos = buffer.get_cur_line(), buffer.current_pos - 1
					local prev_finish = env[buffer].walker.finish_before(range)
					if prev_finish then
						seek(i)(prev_finish)
						same_line = line == buffer.get_cur_line()
						seek(i)(pos)
					end
				end
				if sexp or same_line then
					eval_repl_line(range, buffer.selection_n_caret[i] - 1)
				end
			end
		end
		ret = winput.insert(range, seek(i), key, M.auto_square_brackets)
	end
	if consumed_char then buffer:end_undo_action() end
	return ret
end)

return M
