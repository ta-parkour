ta-parkour is a structured editing plugin for the [Textadept editor](https://github.com/orbitalquark/textadept),
based on the [parkour library](https://repo.or.cz/lisp-parkour.git).

# Configuration

In `~/.textadept/init.lua`:

    local pk = require'ta-parkour'

    -- optional (example values):
    pk.auto_square_brackets = true
    pk.lispwords.scheme.lambda = 0
    pk.repl_fifo = os.getenv('HOME')..'/.repl_fifo'
    pk.emacs = true
    pk.compat = true

## `auto_square_brackets`

Rewrites any delimiter to a square bracket at certain locations.  
Has effect only on Scheme; for Clojure and Fennel this behaviour is always on.  
Works both when inserting and wrapping.  
The locations are not configurable ATM, but are grouped by language in the code - see, for example, the [Fennel config](parkour/cfg-fennel.lua#l24).

## `lispwords`_`.dialect.word`_

The plugin comes with a set of indentation rules for each supported file type,
but they are incomplete and sometimes will be wrong (due to multiple possible dialects under a single file type).  
The `lispwords` table allows customizing those rules by setting the desired number of distinguished arguments
a function/macro has. (`0` is like `defun` in Emacs.)  
As an example, see the built-in indent numbers [for Scheme](parkour/cfg-scheme.lua#l11).

## `repl_fifo`

This option can be set to the path of a named pipe from which a REPL (or anything, really) can read input.  
Inserting a newline at the end of the last line of a paragraph will send the paragraph to this pipe.  
Since REPL commands fit the criteria for paragraph (a top-level S-expression), they get sent as well.

## `emacs`

The plugin comes with two key themes - [emacs](keytheme/emacs.lua) and [CUA](keytheme/CUA.lua).  
`CUA` is always enabled.  
Setting this option to true will enable the `emacs` key theme, too. (A few CUA and built-in key bindings will be overridden.)

## `compat`

Setting this option to true will swap some of the motions in the native key theme with ones more resembling the built-in CUA motions.  
See [CUA-compat](keytheme/CUA-compat.lua).

__Note:__ menu mnemonics conflicting with `Alt+` shortcuts of an active key theme will be disabled in the current buffer.  
__Note:__ The linked key themes are the only "documentation" on the available keyboard shortcuts ATM.
