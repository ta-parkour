return function(B, H)
return {
	-- Structured motions:
	[B.M.forward] = 'ctrl+alt+f',
	[B.M.backward] = 'ctrl+alt+b',
	[B.M.forward_down] = 'ctrl+alt+d',
	[B.M.backward_up] = 'ctrl+alt+u',
	[B.M.forward_up] = 'ctrl+alt+n',
	[B.M.backward_down] = 'ctrl+alt+p',
	[B.M.para_down] = 'ctrl+alt+e',
	[B.M.para_up] = 'ctrl+alt+a',
	[B.M.end_of_sentence] = 'alt+e',
	[B.M.beginning_of_sentence] = 'alt+a',
	[B.M.next_finish_float] = 'alt+f',
	[B.M.prev_start_float] = 'alt+b',
	[B.M.next_section] = {['ctrl+x'] = ']'},
	[B.M.prev_section] = {['ctrl+x'] = '['},
	-- Structured selections:
	[B.T.mark_sexp] = {'ctrl+alt+ ', 'ctrl+alt+@'},
	[B.T.mark_defun] = {'alt+h', 'ctrl+alt+h'},
	[B.T.expand_region] = 'ctrl+=',
	-- Structured deletions:
	[B.I.kill_sexp] = 'ctrl+alt+k',
	[B.I.cut] = 'ctrl+w',  -- backward-kill-sexp
	[B.I.kill_sentence] = 'alt+k',
	[B.I.backward_kill_sentence] = {['ctrl+x'] = '\b'},
	[B.I.forward_kill_word] = 'alt+d',
	[B.I.backward_kill_word] = 'alt+\b',
	[B.I.kill] = 'ctrl+k',
	[B.I.backward_kill_line] = {['alt+0'] = 'ctrl+k'},
	-- Structured operators:
	[B.I.split_sexp] = 'alt+S',
	[B.I.join_sexps] = 'alt+J',
	[B.I.forward_barf] = 'ctrl+}',
	[B.I.forward_slurp] = 'ctrl+)',
	[B.I.backward_barf] = 'ctrl+{',
	[B.I.backward_slurp] = 'ctrl+(',
	[B.I.raise_sexp] = 'alt+r',
	[B.I.convolute_sexp] = 'alt+?',
	[B.I.splice_sexp] = 'alt+s',
	[B.I.wrap_round] = 'alt+(',
	[B.I.wrap_square] = 'alt+[',
	[B.I.wrap_curly] = 'alt+{',
	[B.I.meta_doublequote] = 'alt+"',
	[B.I.wrap_comment] = 'alt+;',
	[B.I.transpose_sexps] = 'ctrl+alt+t',
	[B.I.transpose_words] = 'alt+t',
	[B.I.splice_sexp_killing_forward] = 'alt+down',
	[B.I.splice_sexp_killing_backward] = 'alt+up',
	-- General operators:
	[B.I.forward_delete] = {'ctrl+d', 'del'},
	[B.I.backward_delete] = '\b',
	[B.I.copy] = 'alt+w',
	[H.A.paste_reindent] = 'ctrl+y',
	[buffer.undo] = 'ctrl+/',
	[buffer.redo] = {['ctrl+g'] = 'ctrl+/'},
	[B.I.reindent_defun] = 'alt+q',
	[B.I.transpose_chars] = 'ctrl+t',
	[B.I.open_next_line] = 'alt+\n',
	[buffer.char_right] = 'ctrl+f',
	[buffer.char_left] = 'ctrl+b',
	[buffer.line_down] = 'ctrl+n',
	[buffer.line_up] = 'ctrl+p',
	[buffer.line_end] = 'ctrl+e',
	[buffer.home] = 'ctrl+a',
	[buffer.document_end] = 'alt+>',
	[buffer.document_start] = 'alt+<',
	[B.M.back_to_indentation] = 'alt+m',
	[buffer.swap_main_anchor_caret] = {['ctrl+x'] = 'ctrl+x'},
	[B.I.close_and_newline] = {'alt+)', 'alt+]', 'alt+}'},
	[B.I.eval_defun] = 'ctrl+alt+x',
	[B.I.eval_last_sexp] = {['ctrl+x'] = 'ctrl+e'},
}
end
