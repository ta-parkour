return function(B, H)
return {
	[B.M.next_finish_float] = 'ctrl+right',
	[B.M.prev_start_float] = 'ctrl+left',
	[B.I.kill_sexp1_float] = 'ctrl+del',
	[B.I.backward_kill_sexp1_float] = 'ctrl+\b',
}
end
